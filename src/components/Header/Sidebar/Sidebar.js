import React, { Component } from "react";

import "./Sidebar.scss";

class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        <div className="sidebarUpper">
          <div className="outerCircle">
            <div className="innerCircle">
              <div className="fullCircle" />
            </div>
          </div>
          <svg className="iconUpper" width="16px" height="20px">
            <image href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAUCAMAAACzvE1FAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAXVBMVEX///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8/beDHAAAAHnRSTlMAAUsCmlneZf7Ymx36fByNr7CMzEBBy+8UFe6A50dA8JsVAAAAAWJLR0QAiAUdSAAAAHJJREFUGNOtjNkOwjAMBJ30Lr24oUnn/z+zlU1FQOKNedndsWQRxXnvJMWDT3eWQ569d1GyURb7rmqatm2oK9uHjn776Hq6QcXIZJcjJ80zFxNXbpp3HiaezJqwf3+1P4iQiqA7JQhfyA8ixA+xxLhYWwFPSQt4jkSAZAAAAABJRU5ErkJggg==" />
          </svg>
        </div>
        <div className="sidebarLower">
          <svg className="iconLower" width="16px" height="16px">
            <image href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABlBMVEX///////9VfPVsAAAAAXRSTlMAQObYZgAAAAFiS0dEAIgFHUgAAAARSURBVAjXY/h8ngGOIACvCAD7VxUZ3xYXogAAAABJRU5ErkJggg==" />
          </svg>
        </div>
      </div>
    );
  }
}

export default Sidebar;
