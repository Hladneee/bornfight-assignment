import React, { Component } from "react";

import "./Header.scss";
import blackAndWhiteGuy from "../../img/blackAndWhiteGuy.png";
import Sidebar from "./Sidebar/Sidebar";

class Header extends Component {
  discard() {
    window.location.reload();
  }

  delete = () => {
    if (window.confirm("Do you really want to delete this event?")) {
      this.discard();
    }
  };

  saveButtonSubmit(event) {
    const allData = this.props.allData;
    if (
      (allData.eventName !== "" &&
        allData.eventTime.eventStartDate !== "" &&
        allData.eventTime.eventEndDate !== "" &&
        allData.eventDuration.allDay) ||
      (allData.eventName !== "" &&
        allData.eventTime.eventStartDate !== "" &&
        allData.eventTime.eventEndDate !== "" &&
        allData.eventTime.eventStartTime !== "" &&
        allData.eventTime.eventEndTime !== "" &&
        !allData.eventDuration.allDay)
    ) {
      localStorage.setItem("allData", JSON.stringify(this.props.allData));
      console.log(
        "Data saved to localStorage, console logged data:",
        this.props.allData
      );
    } else {
      alert(
        "You must atleast fill event name, start & end date and/or start & end time."
      );
    }
  }

  render() {
    return (
      <React.Fragment>
        <button className="backButton">
          <svg width="8px" height="12px">
            <image href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAMBAMAAACtsOGuAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAJFBMVEWAgIBSIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ3///+3bpR4AAAACnRSTlMAgPsqVFVW/Fd+qyox0QAAAAFiS0dECx/XxMAAAAArSURBVAjXY2BgYBRgYGCQUgYyFjkAGaogRgKQUQYSrgASjMsb0JjFIOYEAMIjB5b9zeG0AAAAAElFTkSuQmCC" />
          </svg>
        </button>
        <div className="header">
          <input
            type="submit"
            value="Save"
            className="saveButton"
            onClick={this.saveButtonSubmit.bind(this)}
          />
          <p className="discard" onClick={this.discard}>
            <svg className="discardIcon" width="16px" height="16px">
              <image href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAVFBMVEWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMX///8Ex9PQAAAAGnRSTlMAMorN6wqT/g20kDD9fGSM/GFgi2jMaY+zCcU6c4oAAAABYktHRBsCYNSkAAAAdklEQVQY02WP2xKEIAxDA9T7nRVX8/8fuhYcdYfz1IRpSYATY52IswaJoqwYqcpCdd3wpqlPo+WLFujIfhhVjENPdpjIeYEnPZaZ/MDpo8e6qkkGSNzdgC0OkhvZSnY0+/Y/2Fej74/eNTqOp9xx9TU2iIRU/weXuBCMp6jjtAAAAABJRU5ErkJggg==" />
            </svg>
            Discard
          </p>
          <p className="delete" onClick={this.delete}>
            <svg className="deleteIcon" width="16px" height="16px">
              <image href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAOCAMAAAAsYw3eAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAP1BMVEWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMWSdMX///81wVtrAAAAE3RSTlNYZa0vP0IKqdoh0SDLzSLW1Xh16GsfNAAAAAFiS0dEFJLfyTUAAABDSURBVAjXpcpLDoAgDADRKWCx/Pxw/7vqgibund1LBkTCfAsiMD8R07ZKEc37KitWfCpGbY5W/8O6oxvjcJwDvRy3Pg5TCYl6NN6rAAAAAElFTkSuQmCC" />
            </svg>
            Delete
          </p>
          <button className="searchButton">
            <svg className="searchIcon" width="13px" height="13px">
              <image href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAMAAABFNRROAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAt1BMVEWAgID////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////38+1TAAAAPHRSTlMABli+2NGEHASNxpes79knfTXCC73IaEmUL2fSqUNbii7dHhra820wTrafKLnf15AKSmUfjgmTB0UIRhhfBeUZAAAAAWJLR0QB/wIt3gAAAHtJREFUCNdjYGBkYmZhZWNnAAMOThsubh5ePn4wj8lGAEgK8goJg3giomBBMRtxEMUnAeZJ2kiBKGkZME/WRg5EydsogDUoKoEoZRVVNXUNTRstiBXaOjY2NroSevoQroGhkZwxg4kNNwMCmHLbmBkguOYSNhZIspZW1gA35gq8jhd46QAAAABJRU5ErkJggg==" />
            </svg>
          </button>
          <div className="profileDiv">
            <img
              className="profileImg"
              src={blackAndWhiteGuy}
              alt="profileImage"
            />
            <svg className="profileIcon" width="13px" height="13px">
              <image href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFBAMAAACKv7BmAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAALVBMVEWAgIBSIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ1SIJ3///+RzCMSAAAADXRSTlMAObgPubbIx8XMDsQNsR2TeAAAAAFiS0dEDm+9ME8AAAAiSURBVAjXYxAyYHZkiE02v8pgcXNuFwPX3tsLGBhO9DAAAFlSCCFnOtYfAAAAAElFTkSuQmCC" />
            </svg>
          </div>
        </div>
        <Sidebar className="sidebar" />
      </React.Fragment>
    );
  }
}

export default Header;
