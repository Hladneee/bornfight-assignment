import React, { Component } from "react";
import uuidv from "uuid/v4";

import Header from "../Header/Header";
import Event from "../Event/Event";
import "./IndexPage.scss";

class IndexPage extends Component {
  componentDidMount() {
    const notificationUuid = uuidv();
    const newStateNotifications = [...this.state.notifications];
    newStateNotifications[0].notificationId = notificationUuid;
    this.setState({
      ...this.state,
      notifications: newStateNotifications
    });
  }
  state = {
    eventName: "",
    eventDuration: {
      allDay: false,
      repeat: false
    },
    eventTime: {
      eventStartDate: "",
      eventStartTime: "",
      eventEndDate: "",
      eventEndTime: ""
    },
    eventGuests: [], // "InviteOthers: true/false", "ModifyEvent: true/false", "SeeGuestList: true/false"
    eventType: "Default", // "Default", "Public", "Private"
    moreActions: "More actions", // "More actions", "Repeating", "Temporary"
    eventLocation: "",
    eventDescription: "",
    notifications: [
      {
        notificationType: "Email",
        notificationValue: "",
        notificationTime: "Minutes",
        notificationId: ""
      }
    ],
    calendar: {
      user: "Mario Šestak",
      showAs: "Show me as",
      eventColor: ""
    }
  };

  //Change eventName, time, upper checkboxes

  changeEventName = event => {
    this.setState({
      ...this.state,
      eventName: event.target.value
    });
  };

  toggleCheckboxAllDay = event => {
    this.setState({
      ...this.state,
      eventDuration: {
        ...this.state.eventDuration,
        allDay: event.target.checked
      }
    });
  };

  toggleCheckboxRepeat = event => {
    this.setState({
      ...this.state,
      eventDuration: {
        ...this.state.eventDuration,
        repeat: event.target.checked
      }
    });
  };

  changeEventStartTime = event => {
    this.setState({
      ...this.state,
      eventTime: {
        ...this.state.eventTime,
        eventStartTime: event.target.value
      }
    });
  };

  changeEventEndTime = event => {
    this.setState({
      ...this.state,
      eventTime: {
        ...this.state.eventTime,
        eventEndTime: event.target.value
      }
    });
  };

  changeEventStartDate = event => {
    this.setState({
      ...this.state,
      eventTime: {
        ...this.state.eventTime,
        eventStartDate: event.target.value
      }
    });
  };

  changeEventEndDate = event => {
    this.setState({
      ...this.state,
      eventTime: {
        ...this.state.eventTime,
        eventEndDate: event.target.value
      }
    });
  };
  //_________________________________

  // Add guests and notifications

  addGuest = newGuestObj => {
    this.setState({
      ...this.state,
      eventGuests: [...this.state.eventGuests, newGuestObj]
    });
  };

  deleteGuest = (event, id) => {
    const changedGuests = [...this.state.eventGuests];
    const newGuestsObj = changedGuests.filter((_, index) => index !== id);
    this.setState({
      ...this.state,
      eventGuests: newGuestsObj
    });
  };

  addNotificationInput = () => {
    const newNotificationInput = {
      notificationType: "Email",
      notificationValue: "",
      notificationTime: "Minutes",
      notificationId: uuidv()
    };
    this.setState({
      ...this.state,
      notifications: [...this.state.notifications, newNotificationInput]
    });
  };

  changeNotificationType = (event, id) => {
    const changedNotification = [...this.state.notifications];
    changedNotification[id] = {
      ...changedNotification[id],
      notificationType: event.target.value
    };
    this.setState({
      ...this.state,
      notifications: changedNotification
    });
  };

  changeNotificationValue = (event, id) => {
    const changedNotification = [...this.state.notifications];
    changedNotification[id] = {
      ...changedNotification[id],
      notificationValue: event.target.value
    };
    this.setState({
      ...this.state,
      notifications: changedNotification
    });
  };

  changeNotificationTime = (event, id) => {
    console.log(event, id);
    const changedNotification = [...this.state.notifications];
    changedNotification[id] = {
      ...changedNotification[id],
      notificationTime: event.target.value
    };
    this.setState({
      ...this.state,
      notifications: changedNotification
    });
  };

  deleteNotification = (event, id) => {
    console.log(event, id);
    const changedNotification = [...this.state.notifications];
    const newNotificationObj = changedNotification.filter(
      (_, index) => index !== id
    );
    this.setState({
      ...this.state,
      notifications: newNotificationObj
    });
  };

  //_________________________________

  //Change event properties, location description, calendar

  changeEventType = event => {
    this.setState({
      ...this.state,
      eventType: event.target.value
    });
  };

  changeMoreActions = event => {
    this.setState({
      ...this.state,
      moreActions: event.target.value
    });
  };

  changeEventLocation = event => {
    this.setState({
      ...this.state,
      eventLocation: event.target.value
    });
  };

  changeEventDescription = event => {
    this.setState({
      ...this.state,
      eventDescription: event.target.value
    });
  };

  changeCalendarUser = event => {
    this.setState({
      ...this.state,
      calendar: {
        ...this.state.calendar,
        user: event.target.value
      }
    });
  };

  changeShowAs = event => {
    this.setState({
      ...this.state,
      calendar: {
        ...this.state.calendar,
        showAs: event.target.value
      }
    });
  };

  changeEventColor = event => {
    this.setState({
      ...this.state,
      calendar: {
        ...this.state.calendar,
        eventColor: event.target.value
      }
    });
  };

  //_________________________________

  render() {
    return (
      <div className="indexPage">
        <Header allData={this.state} />
        <Event
          changeEventName={this.changeEventName}
          toggleCheckboxAllDay={this.toggleCheckboxAllDay}
          toggleCheckboxRepeat={this.toggleCheckboxRepeat}
          changeEventStartTime={this.changeEventStartTime}
          changeEventEndTime={this.changeEventEndTime}
          changeEventStartDate={this.changeEventStartDate}
          changeEventEndDate={this.changeEventEndDate}
          changeEventType={this.changeEventType}
          changeMoreActions={this.changeMoreActions}
          changeEventLocation={this.changeEventLocation}
          changeEventDescription={this.changeEventDescription}
          changeCalendarUser={this.changeCalendarUser}
          changeShowAs={this.changeShowAs}
          changeEventColor={this.changeEventColor}
          addGuest={this.addGuest}
          deleteGuest={this.deleteGuest}
          eventGuests={this.state.eventGuests}
          notificationsState={this.state.notifications}
          addNotificationInput={this.addNotificationInput}
          changeNotificationType={this.changeNotificationType}
          changeNotificationValue={this.changeNotificationValue}
          changeNotificationTime={this.changeNotificationTime}
          deleteNotification={this.deleteNotification}
          allData={this.state}
        />
      </div>
    );
  }
}

export default IndexPage;
