import React, { Component } from "react";

import "./EventName.scss";

class EventName extends Component {
  formSubmit(event) {
    event.preventDefault();
    localStorage.setItem("allData", JSON.stringify(this.props.allData));
    console.log(
      "Data saved to localStorage, console logged data:",
      this.props.allData
    );
  }

  render() {
    return (
      <div className="eventNameWrapper">
        <form
          className="eventNameForm"
          onSubmit={this.formSubmit.bind(this)}
        >
          <input
            className="eventNameInput"
            type="text"
            required
            onChange={this.props.changeEventName}
          />
          <input className="saveEvent" type="submit" value="Save event" />
          <div className="verticalLine" />
          <div className="timesDiv">
            <input
              className="eventStartTime"
              required
              pattern="[0-9]{2}:[0-9]{2}"
              type="time"
              onChange={this.props.changeEventStartTime}
              disabled={this.props.allData.eventDuration.allDay ? true : false}
            />
            <input
              className="eventEndTime"
              required
              pattern="[0-9]{2}:[0-9]{2}"
              type="time"
              onChange={this.props.changeEventEndTime}
              disabled={this.props.allData.eventDuration.allDay ? true : false}
            />
          </div>
          <div className="datesDiv">
            <input
              className="eventStartDate"
              type="date"
              required
              onChange={this.props.changeEventStartDate}
            />
            <input
              className="eventEndDate"
              type="date"
              required
              onChange={this.props.changeEventEndDate}
            />
          </div>
          <div className="eventOptionCheckboxDiv">
            <label className="allDayLabel" htmlFor="allDay">
              <input
                className="allDay"
                type="checkbox"
                id="allDay"
                onClick={this.props.toggleCheckboxAllDay}
              />
              <span className="eventCheckbox" />
              All day
            </label>
            <label className="repeatLabel" htmlFor="repeat">
              <input
                className="repeat"
                type="checkbox"
                id="repeat"
                onClick={this.props.toggleCheckboxRepeat}
              />
              <span className="eventCheckbox2" />
              Repeat
            </label>
          </div>
          <a className="timezone" href="https://www.timeanddate.com/time/map/">
            Time Zone
          </a>
        </form>
      </div>
    );
  }
}

export default EventName;
