import React, { Component } from "react";

import "./Event.scss";
import EventName from "./EventName/EventName";
import EventGuests from "./EventGuests/EventGuests";
import EventDetails from "./EventDetails/EventDetails";

class Event extends Component {
  render() {
    const {
      changeEventType,
      changeMoreActions,
      changeEventLocation,
      changeEventDescription,
      changeCalendarUser,
      changeShowAs,
      changeEventColor,
      addGuest,
      deleteGuest,
      eventGuests,
      notificationsState,
      addNotificationInput,
      changeNotificationType,
      changeNotificationValue,
      changeNotificationTime,
      deleteNotification,
      ...other /* changeEventName, toggleCheckboxAllDay, toggleCheckboxRepeat, changeEventStartTime,
                  changeEventEndTime, changeEventStartDate, changeEventEndDate, allData */
    } = this.props;
    const eventGuestsProps = {
      changeEventType,
      changeMoreActions,
      addGuest,
      deleteGuest,
      eventGuests
    };
    const eventDetailsProps = {
      changeEventLocation,
      changeEventDescription,
      changeCalendarUser,
      changeShowAs,
      changeEventColor,
      notificationsState,
      addNotificationInput,
      changeNotificationType,
      changeNotificationValue,
      changeNotificationTime,
      deleteNotification
    };

    return (
      <div className="eventWrapper">
        <EventName {...other} />
        <EventGuests {...eventGuestsProps} />
        <div className="eventCardsDiv">
          <button className="eventDetailsButton">Event details</button>
          <button className="findTimeButton">Find a Time</button>
        </div>
        <EventDetails {...eventDetailsProps} />
      </div>
    );
  }
}

export default Event;
