import React, { Component } from "react";
import uuidv from "uuid/v4";

import "./EventGuests.scss";

class EventGuests extends Component {
  state = {
    guestName: "",
    inviteOthers: false,
    modifyEvent: false,
    seeGuestList: false,
    guestId: uuidv()
  };

  changeGuestName = event => {
    this.setState({
      ...this.state,
      guestName: event.target.value
    });
  };

  toggleInviteOthersCheckbox = event => {
    this.setState({
      ...this.state,
      inviteOthers: event.target.checked
    });
  };

  toggleModifyEventCheckbox = event => {
    this.setState({
      ...this.state,
      modifyEvent: event.target.checked
    });
  };

  toggleSeeGuestListCheckbox = event => {
    this.setState({
      ...this.state,
      seeGuestList: event.target.checked
    });
  };

  addGuestToParent = e => {
    if (this.state.guestName !== "") {
      e.preventDefault();
      const newGuest = this.state;
      this.props.addGuest(newGuest);

      //Resetting state and form
      const defaultState = {
        guestName: "",
        inviteOthers: false,
        modifyEvent: false,
        seeGuestList: false,
        guestId: uuidv()
      };
      this.setState({
        ...defaultState
      });
      this.refs.guestForm.reset();
    }
  };

  render() {
    const eventGuests = this.props.eventGuests.map((el, index) => (
      <p
        key={index}
        onClick={event => this.props.deleteGuest(event, index)}
      >{`-${el.guestName}\u00A0\u00A0`}</p>
    ));

    return (
      <div className="eventGuests">
        <h3>Add Guests</h3>
        <form className="guestsForm" id="guestForm" ref="guestForm">
          <button
            className="addGuestButton"
            value="addGuest"
            onClick={this.addGuestToParent}
            disabled={this.props.eventGuests.length < 20 ? false : true}
          >
            Add
          </button>
          <input
            className="addGuestInput"
            type="text"
            required
            onChange={this.changeGuestName}
          />
          <div className="guestsCheckboxDiv">
            <label className="othersLabel" htmlFor="others">
              <input
                className="others"
                type="checkbox"
                id="others"
                onClick={this.toggleInviteOthersCheckbox}
              />
              <span className="otherCheckbox" />
              Invite others
            </label>
            <label className="modifyLabel" htmlFor="modify">
              <input
                className="modify"
                type="checkbox"
                id="modify"
                onClick={this.toggleModifyEventCheckbox}
              />
              <span className="modifyCheckbox" />
              Modify event
            </label>
            <label className="guestListLabel" htmlFor="guestList">
              <input
                className="guestList"
                type="checkbox"
                id="guestList"
                onClick={this.toggleSeeGuestListCheckbox}
              />
              <span className="guestListCheckbox" />
              See guest list
            </label>
          </div>
          <div className="eventGuestListDiv">{eventGuests}</div>
          <div className="horizontalLine" />
        </form>
        <select
          className="eventTypeSelect"
          name="eventType"
          onChange={this.props.changeEventType}
        >
          <option value="Default">Default</option>
          <option value="Public">Public</option>
          <option value="Private">Private</option>
        </select>
        <select
          className="moreActionsSelect"
          name="moreActions"
          onChange={this.props.changeMoreActions}
        >
          <option value="moreActions">More actions</option>
          <option value="Repeating">Repeating</option>
          <option value="Temporary">Temporary</option>
        </select>
        <p className="message">
          By default this event will follow the sharing settings of this
          calendar, event details will be visible to anyone who can see details
          of other events in this calendar. <a href="*">More</a>
        </p>
        <button className="publishEventButton">Publish event</button>
      </div>
    );
  }
}

export default EventGuests;
