import React, { Component } from "react";

import "./EventNotification.scss";

class EventNotification extends Component {
  render() {
    return (
      <React.Fragment>
        <select
          className="eventNotificationSelect"
          name="eventNotification"
          form="eventDetails"
          onChange={event =>
            this.props.changeNotificationType(event, this.props.index)
          }
        >
          <option value="Email">Email</option>
          <option value="Browser alert">Browser alert</option>
        </select>
        <input
          className="numberInput"
          type="number"
          min="0"
          max="2"
          required
          onChange={event =>
            this.props.changeNotificationValue(event, this.props.index)
          }
        />
        <select
          className="eventNotificationTimeSelect"
          name="eventNotificationTime"
          form="eventDetails"
          onChange={event =>
            this.props.changeNotificationTime(event, this.props.index)
          }
        >
          <option value="Minutes">Minutes</option>
          <option value="Hours">Hours</option>
        </select>
        <svg
          className="deleteNotification"
          width="9px"
          height="9px"
          onClick={event => this.props.deleteNotification(event, this.props.index)}
        >
          <path
            fillRule="evenodd"
            fill="rgb(250, 64, 132)"
            d="M8.035,7.328 L7.328,8.035 L4.500,5.207 L1.671,8.035 L0.964,7.328 L3.793,4.500 L0.964,1.671 L1.671,0.964 L4.500,3.793 L7.328,0.964 L8.035,1.671 L5.207,4.500 L8.035,7.328 Z"
          />
        </svg>
      </React.Fragment>
    );
  }
}

export default EventNotification;
