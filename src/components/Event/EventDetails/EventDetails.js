import React, { Component } from "react";

import EventNotifaction from "./EventNotification/EventNotification";
import "./EventDetails.scss";

import plusImg from "../../../img/plus.png";
import darkBlueCircle from "../../../img/darkBlue.png";
import lightBlueCircle from "../../../img/lightBlue.png";
import purpleCircle from "../../../img/purple.png";
import turquoiseCircle from "../../../img/turquoise.png";
import yellowCircle from "../../../img/yellow.png";
import redCircle from "../../../img/red.png";
import whiteCircle from "../../../img/white.png";

class EventDetails extends Component {
  render() {
    const notifications = this.props.notificationsState.map((el, index) => (
      <EventNotifaction
        key={el.notificationId}
        index={index}
        changeNotificationType={this.props.changeNotificationType}
        changeNotificationValue={this.props.changeNotificationValue}
        changeNotificationTime={this.props.changeNotificationTime}
        deleteNotification={this.props.deleteNotification}
      />
    ));

    return (
      <div className="eventDetails">
        <form className="eventDetailsLeft" id="eventDetails">
          <p>Where</p>
          <input
            className="eventLocationInput"
            type="text"
            placeholder="Enter Location of your event"
            required
            onChange={this.props.changeEventLocation}
          />
          <p>Description</p>
          <textarea
            className="descriptionArea"
            required
            onChange={this.props.changeEventDescription}
          />
        </form>
        <div className="verticalLine2" />
        <div className="eventDetailsRight">
          <p className="notificationsParagraph">Notifications</p>
          {notifications}
          {this.props.notificationsState.length <= 4 ? (
            <div
              className="addNotification"
              onClick={this.props.addNotificationInput}
            >
              <img src={plusImg} alt="+" /> Add a notification
            </div>
          ) : null}
          <p className="notificationsParagraph2">Calendar</p>
          <div className="calendarDiv">
            <select
              className="calendarSelect"
              name="calendar"
              form="eventDetails"
              onChange={this.props.changeCalendarUser}
            >
              <option value="Mario Šestak">Mario Šestak</option>
              <option value="Zoran Završki">Zoran Završki</option>
              <option value="Tomislav Grubišić">Tomislav Grubišić</option>
            </select>
            <select
              className="showMeAsSelect"
              name="showMeAs"
              form="eventDetails"
              onChange={this.props.changeShowAs}
            >
              <option value="Show me as">Show me as</option>
              <option value="Owner">Owner</option>
              <option value="Admin">Admin</option>
            </select>
          </div>
          <p className="notificationsParagraph3">Event Color</p>
          <div className="colorDiv">
            <label htmlFor="color1">
              <input
                type="radio"
                id="color1"
                name="color"
                value="darkBlue"
                required
                onChange={this.props.changeEventColor}
              />
              <img src={darkBlueCircle} alt="darkBlue" />
            </label>
            <label htmlFor="color2">
              <input
                type="radio"
                id="color2"
                name="color"
                value="lightBlue"
                onChange={this.props.changeEventColor}
              />
              <img src={lightBlueCircle} alt="lightBlue" />
            </label>
            <label htmlFor="color3">
              <input
                type="radio"
                id="color3"
                name="color"
                value="purple"
                onChange={this.props.changeEventColor}
              />
              <img src={purpleCircle} alt="purple" />
            </label>
            {/*___________________________________________________________________ */}
            <label htmlFor="color4">
              <input
                type="radio"
                id="color4"
                name="color"
                value="turquoise"
                onChange={this.props.changeEventColor}
              />
              <img src={turquoiseCircle} alt="turquoise" />
            </label>
            <label htmlFor="color5">
              <input
                type="radio"
                id="color5"
                name="color"
                value="yellow"
                onChange={this.props.changeEventColor}
              />
              <img src={yellowCircle} alt="yellow" />
            </label>
            <label htmlFor="color6">
              <input
                type="radio"
                id="color6"
                name="color"
                value="red"
                onChange={this.props.changeEventColor}
              />
              <img src={redCircle} alt="red" />
            </label>
            <div className="moreColorsDiv">
              <p>...</p>
              <img src={whiteCircle} alt="white" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EventDetails;
