import React, { Component } from 'react';

import IndexPage from './components/pages/IndexPage';
import './App.scss';


class App extends Component {
  render() {
    return (
      <div className="App">
        <IndexPage/>
      </div>
    );
  }
}

export default App;
